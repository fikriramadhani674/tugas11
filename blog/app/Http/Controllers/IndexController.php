<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function dashboard()
    {
        return view('halaman.index');
    }

    public function form()
    {
        return view('halaman.form');
    }

    public function welcome(Request $request)
    {
        $nama_depan = $request['nama_depan'];
        $nama_belakang = $request['nama_belakang'];

        return view('halaman.welcome', compact('nama_depan', 'nama_belakang'));
    }
}
