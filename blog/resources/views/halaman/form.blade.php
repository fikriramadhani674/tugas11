<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Pendaftaran</title>
</head>

<body>
    <form action="/welcome" method="post">
        @csrf
        <h1>Buat Account Baru!</h1>
        <h2>Sign Up Form</h2>
        <label for="">First Name:</label><br><br>
        <input type="text" name="nama_depan">
        <br><br>
        <label for="">Last Name:</label><br><br>
        <input type="text" name="nama_belakang">
        <br><br>
        <label for="">Gender:</label><br><br>
        <input type="radio" name="gender">Male
        <br>
        <input type="radio" name="gender">Female
        <br>
        <input type="radio" name="gender">Other
        <br><br>
        <label for="">Nationality:</label><br><br>
        <select>
            <option value="indonesian">Indonesian</option>
            <option value="malaysian">Malaysian</option>
            <option value="singapore">Singapore</option>
            <option value="australian">Australian</option>
        </select>
        <br><br>
        <label for="">Language Spoken:</label><br><br>
        <input type="checkbox">Bahasa Indonesia
        <br>
        <input type="checkbox">English
        <br>
        <input type="checkbox">Other
        <br><br>
        <label for="">Bio:</label><br><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br>
        <button type="submit">Sign Up</button>
    </form>
</body>

</html>
